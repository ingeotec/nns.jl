module NNS
abstract Index
abstract Result

abstract DistanceType
# typealias Distance Union{DistanceType,Function}
# typealias Distance DistanceType

# export Index, DistanceType, Distance, Result
export Index, DistanceType, Result
import JSON

include("distances/bits.jl")
include("distances/sets.jl")
include("distances/strings.jl")
include("distances/vectors.jl")
include("distances/documents.jl")
include("res/knn.jl")
include("nns/io.jl")
include("nns/recall.jl")
include("nns/performance.jl")
include("indexes/pivotselection.jl")
include("indexes/seq.jl")
include("indexes/laesa.jl")
include("indexes/pivotselectiontables.jl")
include("indexes/kvp.jl")
include("indexes/knr.jl")
include("indexes/graph.jl")
end
