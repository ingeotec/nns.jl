#  Copyright 2016 Eric S. Tellez <eric.tellez@infotec.mx>
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

export DocumentType, Document, DocumentASCII, DocumentUTF8


type DocumentType
    terms::Vector{Int32}
    weights::Vector{Float32}
end

function (o::AngleDistance)(a::DocumentType, b::DocumentType)
    o.calls += 1
    return acos(sim_cos(a, b))
end

function sim_cos(a::DocumentType, b::DocumentType)
    norm1::Float64 = 0.0
    norm2::Float64 = 0.0
    sum::Float64 = 0.0

    o = a.weights
    @simd for i = 1:length(o)
	@inbounds norm1 += o[i] * o[i]
    end
    o = b.weights
    @simd for i = 1:length(o)
	@inbounds norm2 += o[i] * o[i]
    end

    let i = 1, j = 1, n1=length(a.terms), n2=length(b.terms)
        while i <= n1 && j <= n2
            @inbounds c = cmp(a.terms[i], b.terms[j])
            if c == 0
                @inbounds sum += a.weights[i] * b.weights[j]
                i += 1
                j += 1
            elseif c < 0
                i += 1
            else
                j += 1
            end
        end
    end
    
    M::Float64 = sum / (sqrt(norm1) * sqrt(norm2))
    if M > 1.0
	M = 1.0
    elseif M < -1.0
	M = -1.0
    end
    
    return M
end


# documents
function from_string(::Type{DocumentType}, s::AbstractString)
    ds = split(s)
    n = length(ds) >> 1
    olist = Vector{Int32}(n)
    wlist = Vector{Float32}(n)
    j = 1

    for i=1:n
        olist[i] = parse(Int32, ds[j])
        j += 1
        wlist[i] = parse(Float32, ds[j])
        j += 1
    end
    
    return DocumentType(olist, wlist)
end

function to_string(item::DocumentType)
    n::Int = length(item.object)
    s::Vector{ASCIIString} = Vector{ASCIIString}(n+n)
    j::Int = 1
    for i=1:n
        s[j] = string(item.object[i])
        s[j+1] = string(item.weights[i])
        j += 2
    end
    
    return join(s, ' ')
end
