export sim_common_prefix
export CommonPrefixDistance, HammingDistance, GenericLevenshtein, LevDistance, LcsDistance

"""
sim_common_prefix computes the length of the common prefix among
two strings represented as arrays
"""

function sim_common_prefix{T <: Any}(a::T, b::T)
	len_a::Int = length(a)
	len_b::Int = length(b)
	i::Int = 1
	min_len::Int = min(len_a, len_b)
	@inbounds while i <= min_len && a[i] == b[i]
		i += 1
	end

	return i - 1
end

type CommonPrefixDistance <: DistanceType
	calls::Int
	CommonPrefixDistance() = new(0)
end

function (o::CommonPrefixDistance){T <: Any}(a::T, b::T)
	o.calls += 1
	return sim_common_prefix(a, b) / max(length(a), length(b))
end

"""
dist_levenshtein computes the edit distance between two strings,
this is a low level function. Please use dist_lev
"""
type GenericLevenshtein <: DistanceType
	calls::Int
	icost::Int
	dcost::Int
	rcost::Int
end

"""LevDistance computes the edit distance"""
LevDistance() = GenericLevenshtein(0, 1, 1, 1)

"""LCS computes the distance associated to the longest common subsequence"""
LcsDistance() = GenericLevenshtein(0, 1, 1, 2)

function (o::GenericLevenshtein){T <: Any}(a::T, b::T)
	alen::Int = length(a)
	blen::Int = length(b)

	alen == 0 && return blen
	blen == 0 && return alen

	C::Vector{Int} = Array(0:blen)
	prevA::Int = 0
	for i in 1:alen
		prevA = i
		prevC::Int = C[1]
		j::Int = 1

		while j <= blen
			cost::Int = o.rcost
			@inbounds if a[i] == b[j]
				cost = 0
			end
			@inbounds C[j] = prevA
			j += 1
			@inbounds prevA = min(C[j]+o.dcost, prevA+o.icost, prevC+cost)
			@inbounds prevC = C[j]
		end
		@inbounds C[j] = prevA
	end

	return prevA
end

"""
dist_hamming computes the hamming distance between two slices of integers
"""
type HammingDistance <: DistanceType
	calls::Int
	HammingDistance() = new(0)
end

function (o::HammingDistance){T <: Any}(a::T, b::T)
	o.calls += 1
	if length(a) != length(b)
		throw(ArgumentError("dist_hamming length(a) != length(b)"))
	end
	d::Int = 0
	for i = 1:length(a)
		@inbounds if a[i] != b[i]
			d += 1
		end
	end

	return d
end
