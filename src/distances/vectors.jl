export sim_jaccard, sim_cos, sim_common_prefix, distance
export L1Distance, L2Distance, L2SquaredDistance, LInfDistance, LpDistance, AngleDistance, CosineDissimilarity

""" L1Distance computes the Manhattan's distance """
type L1Distance <: DistanceType
    calls::Int
    L1Distance() = new(0)
end

function (o::L1Distance){T <: Real}(a::Vector{T}, b::Vector{T})
    o.calls += 1
    d::T = zero(T)

    @fastmath @inbounds @simd for i = 1:length(a)
	m = a[i] - b[i]
        d += ifelse(m > 0, m, -m)
    end

    return d
end

""" L2Distance computes the Euclidean's distance """
type L2Distance <: DistanceType
    calls::Int
    # P::Vector{Float32}
    # L2Distance() = new(0, Vector{Float64}(112))
    L2Distance() = new(0)
end

function (o::L2Distance){T <: Real}(a::Vector{T}, b::Vector{T})
    o.calls += 1
    d::T = zero(T)

    @fastmath @inbounds @simd for i = 1:length(a)
        m = a[i] - b[i]
        d += m * m
    end

    return sqrt(d)
end

""" L2SquaredDistance computes the Euclidean's distance but squared """
type L2SquaredDistance <: DistanceType
    calls::Int
    L2SquaredDistance() = new(0)
end

function (o::L2SquaredDistance){T <: Real}(a::Vector{T}, b::Vector{T})
    o.calls += 1
    d::T = zero(T)

    @fastmath @inbounds @simd for i = 1:length(a)
        m = a[i] - b[i]
        d += m * m
    end

    return d
end


""" LInfDistance computes the max distance """
type LInfDistance <: DistanceType
    calls::Int
    LInfDistance() = new(0)
end

function (o::LInfDistance){T <: Real}(a::Vector{T}, b::Vector{T})
    o.calls += 1
    d::T = zero(T)
    
    @fastmath @inbounds @simd for i = 1:length(a)
	m = a[i] - b[i]
        d = max(d, m)
    end
    
    return d
end

"""
dist_lp computes a generic Minkowski's distance
"""
type LpDistance <: DistanceType
    calls::Int
    p::Float32
    LpDistance(p::Float32) = new(0, p)
end

function (o::LpDistance){T <: Real}(a::Vector{T}, b::Vector{T})
    o.calls += 1
    d::T = zero(T)

    @fastmath @inbounds @simd for i = 1:length(a)
	m = abs(a[i] - b[i])
	d += m ^ o.p
    end
    
    return d ^ (1f0 / o.p)
end

type AngleDistance <: DistanceType
    calls::Int
    AngleDistance() = new(0)
end

function (o::AngleDistance){T <: Real}(a::Vector{T}, b::Vector{T})
    o.calls += 1
    m = max(-1, sim_cos(a, b))
    return acos(min(1, m))
end

type CosineDissimilarity <: DistanceType
    calls::Int
    CosineDissimilarity() = new(0)
end

function (o::CosineDissimilarity){T <: Real}(a::Vector{T}, b::Vector{T})
    o.calls += 1
    return -sim_cos(a, b) + 1
end

function sim_cos{T <: Real}(a::Vector{T}, b::Vector{T})
    norm1::T = zero(T)
    norm2::T = zero(T)
    sum::T = zero(T)
    n::Int = length(a)

    @fastmath @inbounds @simd for i = 1:n
        sum += a[i] * b[i]
        norm1 += a[i] * a[i]
        norm2 += b[i] * b[i]
    end

    sum / sqrt(norm1 * norm2)
end
