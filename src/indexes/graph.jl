#  Copyright 2016 Eric S. Tellez <eric.tellez@infotec.mx>
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http:#www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

export LocalSearchAlgorithm, NeighborhoodAlgorithm, LocalSearchIndex, create_aknn

abstract LocalSearchAlgorithm
abstract NeighborhoodAlgorithm

type LocalSearchIndex{T, D <: DistanceType} <: Index
    search_algo::LocalSearchAlgorithm
    neighborhood_algo::NeighborhoodAlgorithm
    db::Array{T,1}
    dist::D
    expected_recall::Float64
    expected_k::Int
    restarts::Int
    beam_size::Int
    montecarlo_size::Int
    candidate_size::Int
    links::Vector{Vector{Int32}}
end

include("graph/opt.jl")
include("graph/fixedneighborhood.jl")
include("graph/logneighborhood.jl")
include("graph/gallopingneighborhood.jl")
include("graph/essencialneighborhood.jl")
include("graph/satneighborhood.jl")
include("graph/galsatneighborhood.jl")
include("graph/ihc.jl")
include("graph/is2014.jl")
include("graph/steadystate.jl")
include("graph/beamsearch.jl")

function getsetup{T, D <: DistanceType}(index::LocalSearchIndex{T, D})
    header = Dict(
                  "search_algo" => string(index.search_algo),
                  "neighborhood_algo" => string(index.neighborhood_algo),
                  "length" => length(index.db),
                  "type" => string(typeof(index)),
                  "expected_recall" => index.expected_recall,
                  "expected_k" => index.expected_k,
                  "restarts" => index.restarts,
                  "beam_size" => index.beam_size,
                  "montecarlo_size" => index.montecarlo_size,
                  "candidate_size" => index.candidate_size,
                  )
end

function save{T, D <: DistanceType}(index::LocalSearchIndex{T, D}, filename::AbstractString)
    saveDB(index.links, "$(filename).links")
    open(filename, "w") do f
        header = getsetup(index)
        write(f, JSON.json(header, 2), "\n")
    end
end

function LocalSearchIndex{T, D <: DistanceType}(filename::AbstractString,
                                                db::Array{T,1},
                                                dist::D)
    links = loadDB(Array{Int32,1}, "$(filename).links")
    h = JSON.parsefile(filename)
    a = eval(parse(h["search_algo"]))
    b = eval(parse(h["neighborhood_algo"]))

    return LocalSearchIndex(a, b, db, dist,
                            h["expected_recall"],
                            h["expected_k"],
                            h["restarts"],
                            h["beam_size"],
                            h["montecarlo_size"],
                            h["candidate_size"],
                            links)
end

function LocalSearchIndex{D <: DistanceType}(T::Type, dist::D;
                                             expected_recall::Float64=0.9,
                                             expected_k::Int=1,
                                             search_algo::LocalSearchAlgorithm=nothing,
                                             neighborhood_algo::NeighborhoodAlgorithm=nothing,
                                         )
    links = Array(Vector{Int32}, 0)
    db = Array(T, 0)
    if search_algo == nothing
        search_algo = SteadyStateSearch()
    end

    if neighborhood_algo == nothing
        neighborhood_algo = GallopingNeighborhood(1.5, 0.666, 0.9, 1.1)
    end
    
    return LocalSearchIndex(search_algo, neighborhood_algo, db, dist, expected_recall, expected_k, 1, 1, 1, 1, links)
end

function LocalSearchIndex{T, D <: DistanceType}(db::Vector{T}, dist::D;
                                                search_algo::LocalSearchAlgorithm=nothing,
                                                neighborhood_algo::NeighborhoodAlgorithm=nothing,
                                                expected_recall::Float64=0.9,
                                                expected_k::Int=1,
                                                prolog::Function=(I)->I)
    index = LocalSearchIndex(T,
                             dist,
                             expected_recall=expected_recall,
                             expected_k=expected_k,
                             search_algo=search_algo,
                             neighborhood_algo=neighborhood_algo
                             )

    prolog(index)

    for item in db
        push!(index, item)
    end

    optimize!(index)
    return index
end

function search{T, R <: Result}(index::LocalSearchIndex{T}, q::T, res::R)
    search(index.search_algo, index, q, res)
    return res
end

function search{T}(index::LocalSearchIndex{T}, q::T)
    return search(index, q, NnResult())
end

function optimize!{T}(index::LocalSearchIndex{T}; recall::Float64=index.expected_recall, k::Int=index.expected_k)
    optimize_algo!(index.search_algo, index, recall, k)
end

function push!{T}(index::LocalSearchIndex{T}, item::T)
    n::Int = length(index.db)
    k::Int = ceil(Int, log(1.5, 1+n))
    k_1::Int = ceil(Int, log(1.5, 2+n))

    if n > 4 && k != k_1
        optimize!(index)
    end

    if n == 0
        push!(index.links, Int32[])
    else
        L = neighborhood(index.neighborhood_algo, index, item)
        for objID in L
            push!(index.links[objID], 1+n)
        end
        n % 5000 == 0 && info("added n=$(n+1), neighborhood=$(length(L)), $(now())")
        push!(index.links, L)
    end
    
    push!(index.db, item)
   
end

function create_aknn{T}(index::LocalSearchIndex{T}; k=index.expected_k, recall=index.expected_recall)
    optimize!(index, recall=recall, k=k)
    n=length(index.db)
    aknn = [Vector{Int32}(0) for i=1:n]
    for i=1:n
        for t in search(index, index.db[i], KnnResult(k))
            push!(aknn[i], t.objID)
            push!(aknn[t.objID], i)

        end
        (i % 10000) == 1 && info("algorithm=$(index.search_algo), neighborhood_factor=$(index.neighborhood_algo), expected-recall=$(recall), k=$(k); advance $i of n=$(length(index.db))")
    end
    newindex = LocalSearchIndex(index.search_algo, index.neighborhood_algo, index.db, index.dist, index.expected_recall, index.expected_k, index.restarts, index.beam_size, index.montecarlo_size, index.candidate_size, aknn)
    optimize!(newindex, recall=recall, k=k)
    return newindex
end
