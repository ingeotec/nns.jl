#  Copyright 2016 Eric S. Tellez <eric.tellez@infotec.mx>
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

export BeamSearch

type BeamSearch <: LocalSearchAlgorithm
end

### local search algorithm

#function first_beam{T, R <: Result}(index::LocalSearchIndex{T}, q::T, res::R, tabu::Set{Int32}, choose::Int, montecarlo::Int)
function first_beam{T, R <: Result}(index::LocalSearchIndex{T}, q::T, res::R, tabu::BitVector, choose::Int, montecarlo::Int)
    # beam = KnnResult(choose)
    beam = SlugKnnResult(choose)
    n::Int32 = length(index.db)
    montecarlo = max(choose, montecarlo)  ## it has no sense that montecarlo < choose
    
    for nodeID in rand(Int32(1):n, montecarlo)
        # @inbounds if !in(nodeID, tabu)
        @inbounds if !tabu[nodeID]
            d = convert(Float32, index.dist(index.db[nodeID], q))
            # push!(tabu, nodeID)
            tabu[nodeID] = true
            push!(res, nodeID, d)
            push!(beam, nodeID, d)
        end
    end

    beam
end

function beam_search{T, R <: Result}(index::LocalSearchIndex{T}, q::T, res::R, tabu::BitVector)
    # first beam
    beam::SlugKnnResult = first_beam(index, q, res, tabu, abs(index.beam_size), abs(index.montecarlo_size))
    new_beam::SlugKnnResult = SlugKnnResult(maxlength(beam))
    cov::Float32 = -1.0
    while cov != last(res).dist
        clear!(new_beam)
        cov = last(res).dist
        for node in beam
            @inbounds for childID in index.links[node.objID]
                # if !in(childID, tabu)
                if !tabu[childID]
                    d = convert(Float32, index.dist(index.db[childID], q))
                    tabu[childID] = true
                    push!(new_beam, childID, d)
                    push!(res, childID, d)
                end
            end
        end

        beam, new_beam = new_beam, beam
    end
    beam
end

function search{T, R <: Result}(algorithm::BeamSearch, index::LocalSearchIndex{T}, q::T, res::R)
    if length(index.db) == 0
        return res
    end

    tabu = falses(length(index.db))
    beam_search(index, q, res, tabu)
    return res
end

function optimize_algo!{T}(algo::BeamSearch, index::LocalSearchIndex{T}, recall::Float64, k::Int=1, numqueries::Int=128)
    n = length(index.db)
    perf = Performance(index.db, index.dist, numqueries, k)
    optimize_neighborhood!(index.neighborhood_algo, index, perf, recall)
    tabu = Set{Tuple{Int,Int}}() # montecarlo_size, beam_size

    best_beam_size::Int = index.beam_size  # index.beam_size
    best_montecarlo_size::Int = index.montecarlo_size
    best_fitness::Float64 = 0.0
    sqrlog = ceil(Int, log2(n)^2)
    local p

    while true
        local_best_fitness = best_fitness
        for beam_size in param_gen_transitions(best_beam_size)
            beam_size = min(beam_size, sqrlog)

            for montecarlo_size in param_gen_transitions(best_montecarlo_size)
                montecarlo_size = min(montecarlo_size, sqrlog)
                code = (montecarlo_size, beam_size)

                if !in(code, tabu)
                    push!(tabu, code)
                    index.beam_size = beam_size
                    index.montecarlo_size = montecarlo_size
                    p = probe(perf, index)
                    fitness = fitness_performance(recall, p)
                    info("$(getsetup(index)), n=$(length(index.db)), state(montecarlo_size, beam_size)=$(code), performance(recall, seconds, distances)=$(p), fitness:$(fitness), best-fitness: $(best_fitness)")
                    if fitness > best_fitness
                        best_fitness = fitness
                        best_beam_size = beam_size
                        best_montecarlo_size = montecarlo_size
                    end
                end
            end
        end
        if local_best_fitness == best_fitness
            break
        end
    end

    index.beam_size = best_beam_size
    index.montecarlo_size = best_montecarlo_size
    info("DONE $(getsetup(index)), n=$(length(index.db)), best-fitness: $(best_fitness)")
    return index
end
