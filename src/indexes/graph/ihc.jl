#  Copyright 2016 Eric S. Tellez <eric.tellez@infotec.mx>
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http:#www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

export IteratedHillClimbingSearch

type IteratedHillClimbingSearch <: LocalSearchAlgorithm
end


function greedy_search_with_tabu{T, R <: Result}(index::LocalSearchIndex{T}, q::T, res::R, tabu::BitVector, nodeID::Int32)
    omin::Int=-1
    dmin::Float32 = typemax(Float32)
    d::Float32 = convert(Float32, index.dist(index.db[nodeID], q))
    push!(tabu, nodeID)
    # push!(evaluated, nodeID)
    push!(res, nodeID, d)
    while true
        dmin = typemax(Float32)
        omin = -1
        @inbounds for childID in index.links[nodeID]
            # if !in(childID, tabu)
            if ! tabu[childID]
                d = convert(Float32, index.dist(index.db[childID], q))
                # push!(tabu, childID)
                tabu[childID] = true
                # min_dist = min(d, min_dist)
                push!(res, childID, d)
                if d < dmin
                    dmin = d
                    omin = childID
                end
            end
        end

        if omin < 0
            break
        else
            nodeID = omin
        end
    end
end

function search{T, R <: Result}(algorithm::IteratedHillClimbingSearch, index::LocalSearchIndex{T}, q::T, res::R)
    # tabu = Set{Int32}()  # items being tabu (already expanded and visited)
    n = length(index.db)
    tabu = falses(n)
    restarts = min(abs(index.restarts), n)

    for i=1:restarts
        nodeID::Int32 = rand(1:n)
        if !in(nodeID, tabu)
            greedy_search_with_tabu(index, q, res, tabu, nodeID)
        end
    end

    return res
end

## optimizing iterated methods

function optimize_algo!{T}(algo::LocalSearchAlgorithm,
                           index::LocalSearchIndex{T},
                           recall::Float64, k::Int=1, numqueries::Int=128)
    max_restarts = Int(ceil(log2(length(index.db)))^2)
    perf = Performance(index.db, index.dist, numqueries, k)
    optimize_neighborhood!(index.neighborhood_algo, index, perf, recall)
    
    tabu = Set{Int}()
    best_fitness = 0.0
    best_restarts = index.restarts
    local p
    
    while true
        local_best_fitness = best_fitness
        for r in param_gen_transitions(best_restarts)
            r = min(r, max_restarts)
            if !in(r, tabu)
                push!(tabu, r)
                index.restarts = r
                p = probe(perf, index)
                fitness = fitness_performance(recall, p)
                info("$(getsetup(index)), n=$(length(index.db)), state(restarts)=$(r), performance(recall, seconds, distances)=$(p), fitness:$(fitness), best-fitness: $(best_fitness)")
                if fitness > best_fitness
                    best_fitness = fitness
                    best_restarts = r
                end
            end
        end
        
        if local_best_fitness == best_fitness
            break
        end
    end
    
    index.restarts = best_restarts
    info("DONE $(getsetup(index)), n=$(length(index.db)), best-fitness: $(best_fitness)")
    return index
end
