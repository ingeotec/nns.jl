export LogarithmicNeighborhood

type LogarithmicNeighborhood <: NeighborhoodAlgorithm
    baselog::Float32
end

function LogarithmicNeighborhood()
    return LogarithmicNeighborhood(2)
end

function optimize_neighborhood!{T}(algo::LogarithmicNeighborhood, index::LocalSearchIndex{T}, perf, recall)
end

function neighborhood{T}(algo::LogarithmicNeighborhood, index::LocalSearchIndex{T}, item::T)
    n = length(index.db)

    _log = log(algo.baselog, n)
    k = max(1, ceil(Int, _log))
    
    knn = search(index, item, KnnResult(k))
    nbuffer::Vector{Int32} = Vector{Int32}(length(knn))

    for (i, p) in enumerate(knn)
        nbuffer[i] = p.objID
    end

    return nbuffer
end
