function param_gen_transitions(p::Int)
    # It generates the neighborhood of p in terms of positive parameters
    # If p is negative, then it doesn't generate transitions
    # (useful to set fixed parameter values)
    # the values of the neighborhood are just values determine the
    # speed of convergence, i.e., putting p +- some-step could result in a
    # slow convergence
    if p < 0
        return p
    else
        # return Int[max(1, Int(ceil(c))) for c in [p / 2, p / 1.5, p, p * 1.5, p * 2]]
        return Int[max(1, Int(ceil(c))) for c in [p / 1.5, p, p * 1.5]]
    end
end

function fitness_performance(expected_recall::Float64, p::PerformanceResult)
    # computes the fitness of a given performance
    # expected_recall determines the minimum recall after reaching expected_recall,
    # the number of queries per second becomes the major parameter
    # a sigmoid function could be of use, but the parameters
    # (i.e., recall and 1/p.seconds) should have similar proportions
    if p.recall < expected_recall
        return p.recall
    else
        return 10.0 + 1.0 / p.seconds
        # return 10.0 + 1.0 / p.distances
    end
end

