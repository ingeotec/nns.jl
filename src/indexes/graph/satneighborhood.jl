export SatNeighborhood

type SatNeighborhood <: NeighborhoodAlgorithm
    k::Int
end

function SatNeighborhood()
    return SatNeighborhood(64)
end

function optimize_neighborhood!{T}(algo::SatNeighborhood, index::LocalSearchIndex{T}, perf, recall)
end

function neighborhood{T}(algo::SatNeighborhood, index::LocalSearchIndex{T}, item::T)
    knn = search(index, item, KnnResult(algo.k))
    N = Vector{Int32}(0)

    for p in knn
        dqp = p.dist
        pobj = index.db[p.objID]
        near = NnResult()
        push!(near, p.objID, p.dist)
        for nearID in N
            @inbounds d = convert(Float32, index.dist(index.db[nearID], pobj)) 
            push!(near, nearID, d)
        end

        f = first(near)
        if f.objID == p.objID
            push!(N, p.objID)
        end

    end
    return N
end
