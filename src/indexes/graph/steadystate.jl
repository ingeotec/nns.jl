#  Copyright 2016 Eric S. Tellez <eric.tellez@infotec.mx>
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

export SteadyStateSearch

type SteadyStateSearch <: LocalSearchAlgorithm
end

###
### Steady state local search
###

# function steady_state_greedy_search_with_tabu{T, R <: Result}(index::LocalSearchIndex{T}, q::T, res::R, tabu::Set{Int32}, candidates::KnnResult)
function steady_state_greedy_search_with_tabu{T, R <: Result}(index::LocalSearchIndex{T}, q::T, res::R, tabu::BitVector, candidates::SlugKnnResult)
    #while length(candidates) > 0 && first(candidates).dist < covrad(res)
    while length(candidates) > 0
        cov = last(res).dist
        nodeID::Int32 = shift!(candidates).objID
        @inbounds for childID in index.links[nodeID]
            # if !in(childID, tabu)
            if !tabu[childID]
                d = convert(Float32, index.dist(index.db[childID], q))
                # push!(tabu, childID)
                tabu[childID] = true
                push!(res, childID, d)
                push!(candidates, childID, d)
            end
        end

        if cov == last(res).dist
            break
        end
    end
end

function search{T, R <: Result}(algorithm::SteadyStateSearch, index::LocalSearchIndex{T}, q::T, res::R)
    # tabu = Set{Int32}()
    n = length(index.db)
    tabu = falses(n)
    restarts = min(abs(index.restarts), n)
    candidates::SlugKnnResult = first_beam(index, q, res, tabu, abs(index.candidate_size), abs(index.montecarlo_size))

    for i=1:restarts
        # while length(candidates) > 0
        # while length(candidates) > 0 && first(candidates).dist < covrad(res)
        length(candidates) == 0 && break
        steady_state_greedy_search_with_tabu(index, q, res, tabu, candidates)
    end
    #end

    return res
end

function optimize_algo!{T}(algo::SteadyStateSearch, index::LocalSearchIndex{T}, recall::Float64, k::Int=1, numqueries::Int=128)
    n = length(index.db)
    perf = Performance(index.db, index.dist, numqueries, k)
    optimize_neighborhood!(index.neighborhood_algo, index, perf, recall)
    tabu = Set{Tuple{Int, Int, Int}}()  # restarts, candidate_size, montecarlo_size
    
    best_restarts = index.restarts
    best_candidate_size = index.candidate_size
    best_montecarlo_size = index.montecarlo_size
    best_fitness::Float64 = -1.0
    sqrlog = ceil(Int, log2(n)^2)
    local p

    while true
        local_best_fitness = best_fitness = 0.0
        for r in param_gen_transitions(best_restarts)
            r = min(r, sqrlog)

            for candidate_size in param_gen_transitions(best_candidate_size)
                candidate_size = min(candidate_size, sqrlog)

                for montecarlo_size in param_gen_transitions(best_candidate_size)
                    montecarlo_size = min(montecarlo_size, sqrlog)

                    code = (r, candidate_size, montecarlo_size)
                    
                    if !in(code, tabu)
                        push!(tabu, code)
                        index.restarts = r
                        index.candidate_size = candidate_size
                        index.montecarlo_size = montecarlo_size
                        
                        p = probe(perf, index)
                        fitness = fitness_performance(recall, p)
                        if fitness > best_fitness
                            best_fitness = fitness
                            best_restarts = r
                            best_candidate_size = candidate_size
                            best_montecarlo_size = montecarlo_size
                        end
                        info("$(getsetup(index)), n=$(length(index.db)), state(restarts, candidate_size, montecarlo_size)=$(code), performance(recall, seconds, distances)=$(p), fitness:$(fitness), best-fitness: $(best_fitness)")
                    end
                end
            end
        end
        
        if local_best_fitness == best_fitness
            break
        end
    end
    index.restarts = best_restarts
    index.candidate_size = best_candidate_size
    index.montecarlo_size = best_montecarlo_size

    info("DONE $(getsetup(index)), n=$(length(index.db))")
    return index
end
