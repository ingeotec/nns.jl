#  Copyright 2016 Eric S. Tellez <eric.tellez@infotec.mx>
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http:#www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

export from_string, to_string, loadDB, saveDB, loadGloveDB

function from_string{T <: Real}(::Type{Array{T,1}}, s::AbstractString)
    @inbounds X = T[parse(T, x) for x in split(s)]
    return X
end

function to_string{T <: Real}(item::Array{T,1})
    return join([string(x) for x in item], " ")
end

function from_string{T <: AbstractString}(::Type{T}, s::AbstractString)
    return s
end

function to_string{T <: AbstractString}(item::T)
    return item
end

function loadDB(t::Type, filename::AbstractString)
    db = Array(t, 0)
    f = open(filename)
    for (i, line) in enumerate(eachline(f))
        i % 10000 == 0 && info("reading $(filename), advance  $(i)")
        x = from_string(t, line)
        push!(db, x)
    end
    close(f)
    return db
end

# Warning: Not every dataset with type Array{T, 1} will be a matrix

function saveDB(db::AbstractArray, filename::AbstractString)
    f = open(filename, "w")
    for (i, item) in enumerate(db)
        i % 10000 == 0 && info("saving $(filename), advance  $(i) of $(length(db))")
        write(f, to_string(item), "\n")
    end
    close(f)
end

function loadGloveDB(filename::AbstractString)
    words = Array(AbstractString, 0)
    db = Array(Array{Float32,1}, 0)

    open(filename) do f
        for (i, line) in enumerate(eachline(f))
            i % 10000 == 0 && info("GloveDB: reading $(filename), advance  $(i)")
            arr = split(line)
            @inbounds item = Float32[parse(Float64, arr[i]) for i=2:length(arr)]
            push!(words, arr[1])
            push!(db, item)
        end
    end

    return (words, db)
end
