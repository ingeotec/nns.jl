#  Copyright 2016  Eric S. Tellez <eric.tellez@infotec.mx>
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

export Performance, PerformanceResult, probe

type PerformanceResult
    recall::Float64
    seconds::Float64
    distances::Float64
end

type Performance{T, R <: Result}
    db::Array{T,1}
    querySet::Array{T,1}
    results::Array{R,1}
    expected_k::Int
    shift_expected_k::Int
end

function Performance{T, D <: DistanceType}(db::Array{T,1}, dist::D, querySet::Array{T,1}, expected_k::Int)
    results = Array(Result, length(querySet))
    s = Sequential(db, dist)
    for i in 1:length(querySet)
        results[i] = search(s, querySet[i], KnnResult(expected_k))
    end

    return Performance(db, querySet, results, expected_k, 0)
end

function Performance{T, D <: DistanceType}(db::Array{T,1}, dist::D, numQueries::Int, expected_k::Int)
    querySet = rand(db, numQueries)
    expected_k += 1  # necessary since we are using items from the same dataset
    results = Array(Result, numQueries)
    s = Sequential(db, dist)
    for i in 1:length(querySet)
        results[i] = search(s, querySet[i], KnnResult(expected_k))
    end

    return Performance(db, querySet, results, expected_k, 1)
end

function probe(perf::Performance, index::Index; measureRecallByDistance::Bool=false)
    p::PerformanceResult = PerformanceResult(0.0, 0.0, 0.0)
    m::Int = length(perf.querySet)

    for i = 1:m
        q = perf.querySet[i]
        start_calls = index.dist.calls
        start_time = time()
        res = search(index, q, KnnResult(perf.expected_k))
        p.seconds += time() - start_time
        p.distances += index.dist.calls - start_calls
        
        base_res = perf.results[i]
        if measureRecallByDistance
            p.recall += generic_recall([item.dist for item in base_res], [item.dist for item in res], perf.shift_expected_k)
        else
            p.recall += generic_recall([item.objID for item in base_res], [item.objID for item in res], perf.shift_expected_k)
        end
    end

    p.recall = p.recall / m
    p.seconds = p.seconds / m
    p.distances = p.distances / m
    return p
end
